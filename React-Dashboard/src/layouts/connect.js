import openSocket from "socket.io-client";
const socket = openSocket("http://192.168.0.19:3000/");

function connect(cb) {
  // listen for any messages coming through
  // of type 'chat' and then trigger the
  // callback function with said message
  socket.on("data", (data) => {

    // console.log the message for posterity
    //console.log(data);
    // trigger the callback passed in when
    // our App component calls connect
    cb(data);
  });
}

export { connect };